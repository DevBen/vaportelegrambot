import XCTest
@testable import vaporTelegramBotTests

XCTMain([
    testCase(vaporTelegramBotTests.allTests),
])
