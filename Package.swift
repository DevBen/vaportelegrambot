// swift-tools-version:3.1

import PackageDescription

let package = Package(
    name: "vaporTelegramBot",
    dependencies: [
        .Package(url: "https://github.com/vapor/vapor.git", majorVersion: 2)
    ]
)
