//
//  Created by Ben Koska on 8/26/17.
//

import Foundation
import Vapor
import HTTP

public final class TelegramSendApi {
    
    public static let shared = TelegramSendApi()
    
    public func sendMessageUrl(token: String, method: String) -> String {
        return "https://api.telegram.org/bot\(token)/\(method)"
    }
    
    public var defaultHeaders: [HeaderKey: String] {
        return [HeaderKey.contentType: "application/json"]
    }

    
    public func send(client: ClientFactoryProtocol = EngineClientFactory(), sendMessage: TelegramSendMessage, token: String) throws -> Response {
        let url = sendMessageUrl(token: token, method: "sendMessage")
        
        let req = Request(method: .post, uri: url, headers: defaultHeaders)
        req.json = try sendMessage.makeJSON()
        
        return try client.respond(to: req)
    }
}
