//
// Created by Ben Koska on 8/27/17.
//

import Foundation
import JSON

public final class TelegramSendMessage: TelegramJSONConvertible {

    // MARK: - Keys

    public struct Keys: TelegramJSONConvertibleKeys {

        public static let chatIdKey = "chat_id"
        public static let textKey = "text"
        public static let parseModeKey = "parse_mode"
        public static let disableWebPagePreviewKey = "disable_web_page_preview"
        public static let disableNotificationKey = "disable_notification"
        public static let replyToMessageIdKey = "reply_to_message_id"
        public static let replyMarkupKey = "reply_markup"
    }

    // MARK: - Primitive types

    /// Unique identifier for the target chat or username of the target channel (in the format @channelusername)
    public var chatId: String

    /// Text of the message to be sent
    public var text: String

    public var parseMode: TelegramParseMode?

    /// Disables link previews for links in this message
    public var disableWebPagePreview: Bool?

    /// Sends the message silently. Users will receive a notification with no sound.
    public var disableNotification: Bool?

    /// If the message is a reply, ID of the original message
    public var replyToMessageId: Int?

    public var replyMarkup: TelegramInlineKeyboardMarkup?

    public init (chatId: String, text: String, parseMode: TelegramParseMode?, disableWebPagePreview: Bool?, disableNotification: Bool?, replyToMessageId: Bool?, replyToMessageId: Int?, replyMarkup: TelegramInlineKeyboardMarkup?) {
        self.chatId = chatId
        self.text = text
        self.parseMode = parseMode
        self.disableWebPagePreview = disableWebPagePreview
        self.disableNotification = disableNotification
        self.replyMarkup = replyMarkup
        self.replyToMessageId = replyToMessageId
        self.replyMarkup = replyMarkup
    }

    public func makeJSON() throws -> JSON {
        var json = JSON()

        try json.set(Keys.chatIdKey, chatId)
        try json.set(Keys.textKey, text)

        if let parseMode = parseMode {
            try json.set(Keys.parseModeKey, parseMode)
        }

        if let disableWebPagePreview = disableWebPagePreview {
            try json.set(Keys.disableWebPagePreviewKey, disableWebPagePreview)
        }

        if let disableNotification = disableNotification {
            try json.set(Keys.disableNotificationKey, disableNotification)
        }

        if let replyToMessageId = replyToMessageId {
            try json.set(Keys.replyToMessageIdKey, replyToMessageId)
        }

        if let replyMarkup = replyMarkup {
            try json.set(Keys.replyMarkupKey, replyMarkup)
        }

        return json
    }
}

public enum TelegramParseMode: String {
    case markdown = "Markdown"
    case html = "HTML"
}
