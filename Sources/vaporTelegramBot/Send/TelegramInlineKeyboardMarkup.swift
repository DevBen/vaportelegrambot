//
//  Created by Ben Koska on 8/27/17.
//

import Foundation
import JSON

public final class TelegramInlineKeyboardMarkup: JSONConvertible {
    public var inline_keyboard: [[InlineKeyboardButton]]

    public init (inline_keyboard: [[InlineKeyboardButton]]) {
        self.inline_keyboard = inline_keyboard
    }
    
    public func makeJSON() throws -> JSON {
        var json = JSON()

        var inline_keyboard_new: [[JSON]]

        for row in inline_keyboard {
            var inline_keyboard_row: [JSON]
            var currentbutton: InlineKeyboardButton
            for button in row {
                currentbutton = try button.makeJSON()
                inline_keyboard_row.append(currentbutton)
            }
            inline_keyboard_new.append(inline_keyboard_row)
        }


        try json.set("inline_keyboard", inline_keyboard_new)
        
        return json
    }
    
}

public final class InlineKeyboardButton: JSONConvertible {

    public var text: String
    public var url: String?
    public var callback_data: String?

    public init (text: String, url: String) {
        self.text = text
        self.url = url
    }

    public init (text: String, callback_data: String) {
        self.text = text
        self.callback_data = callback_data
    }
}

