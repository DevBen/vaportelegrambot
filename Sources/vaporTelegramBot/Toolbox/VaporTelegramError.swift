//
// Created by Ben Koska on 8/26/17.
//
import Foundation

public enum VaporTelegramError: Error {

    case requiredParameterMissing(parameter: String)
    case requiredParameterHasWrongType(parameter: String, neededType: String)
}
