//
// Created by Ben Koska on 8/26/17.
//

import Foundation
import JSON

/// Internal helper which converts json arrays to the specified type
extension JSONRepresentable {
    
    func makeArray<T: JSONInitializable>() throws -> [T]? {
        guard let elements = try makeJSON().array else {
            return nil
        }
        
        var selfArray = [T]()
        for e in elements {
            selfArray.append(try T.init(json: e))
        }
        
        return selfArray
    }
}
